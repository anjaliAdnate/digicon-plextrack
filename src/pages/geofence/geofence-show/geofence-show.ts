import { NavParams } from "ionic-angular";
import { Component, OnInit } from "@angular/core";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
declare var google;

@Component({
    templateUrl: './geofence-show.html',
    styles: [`
    #mapShowGeofence{
        height: 100%;
         width: 100%;
     }`]
})
export class GeofenceShowPage implements OnInit {
    items: any;
    gefence: any;
    gefencename: any;
    geofenceShowdata: any;
    geoshape: any = {};
    GeoFencCoords: any;

    constructor(navParam: NavParams, public apiCall: ApiServiceProvider) {
        this.items = navParam.get("param");
        this.gefence = this.items._id;
        this.gefencename = this.items.geoname;
        console.log(this.gefencename);
    }

    ngOnInit() {
        this.getgeofence();
    }

    getgeofence() {

        console.log("getgeofence");

        var baseURLp = 'http://51.38.175.41/geofencing/getdevicegeofence?gid=' + this.gefence;

        this.apiCall.startLoading().present();
        this.apiCall.getdevicegeofenceCall(baseURLp)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this.geofenceShowdata = data;
                this.geofenceShowdata = data[0].geofence.coordinates[0];
                console.log(this.geofenceShowdata);

                this.geoshape = [];

                this.geoshape = this.geofenceShowdata.map(function (p) {
                    return {
                        lat: p[1], lng: p[0],
                    };
                });
                // console.log("geoshape1=> " + this.geoshape);
                // console.log("geoshape2=> " + JSON.stringify(this.geoshape));
                var temp = JSON.stringify(this.geoshape);
                var temp2 = JSON.parse(temp);
                console.log(temp2)

                this.draw(temp2);
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log(err)
                });
    }

    draw(zzz) {
        var map;
        var infoWindow;
        this.GeoFencCoords = zzz;
        var GeoName = "yyy";

        function initMap(GeoFencCoords) {
            map = new google.maps.Map(document.getElementById('mapShowGeofence'), {
                zoom: 17,
                center: { lat: GeoFencCoords[0].lat, lng: GeoFencCoords[0].lng },
                mapTypeId: 'terrain'
            });
            // console.log("polygon => " + JSON.stringify(GeoFencCoords))
            var bermudaTriangle = new google.maps.Polygon({
                paths: GeoFencCoords,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 3,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });
            bermudaTriangle.setMap(map);

            // Add a listener for the click event.
            bermudaTriangle.addListener('click', showArrays);

            infoWindow = new google.maps.InfoWindow;


        }

        /** @this {google.maps.Polygon} */
        function showArrays(event) {

            // var vertices = this.getPath();

            var contentString = '<b>Geo Fence</b><br>' +
                'Name: ' + GeoName;

            infoWindow.setContent(contentString);
            infoWindow.setPosition(event.latLng);
            infoWindow.open(map);
        }

        initMap(this.GeoFencCoords);
    }
}