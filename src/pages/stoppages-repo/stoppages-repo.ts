import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-stoppages-repo',
  templateUrl: 'stoppages-repo.html',
})
export class StoppagesRepoPage implements OnInit {

  stoppagesReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  arrivalTime: string;
  departureTime: string;
  Durations: string;
  Stoppagesdata: any;
  datetime: number;
  selectedVehicle: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallStoppages: ApiServiceProvider,
    public alertCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    // var temp = new Date();
    // this.datetimeStart = temp.toISOString();
    // // $rootScope.datetimeStart.setHours(0,0,0,0);
    // this.datetimeEnd = temp.toISOString();

    var temp = new Date();

    // this.datetimeStart = temp.toISOString();
    var settime = temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    this.datetime = new Date(settime).setHours(5, 30, 0);

    this.datetimeStart = new Date(this.datetime).toISOString();

    var a = new Date()
    a.setHours(a.getHours() + 5);
    a.setMinutes(a.getMinutes() + 30);
    this.datetimeEnd = new Date(a).toISOString();
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }
  ngOnInit() {
    this.getdevices();
  }


  getdevices() {
    this.apicallStoppages.startLoading().present();
    this.apicallStoppages.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.devices = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicallStoppages.stopLoading();
          console.log(err);
        });
  }


  getStoppagesdevice(from, to, selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle._id;
    // this.getIgnitiondeviceReport(from, to);
  }


  getStoppageReport(starttime, endtime) {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    // console.log(starttime);
    // console.log(endtime);

    this.apicallStoppages.startLoading().present();

    this.apicallStoppages.getStoppageApi(starttime, endtime, this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.stoppagesReport = data;
        // console.log(this.stoppagesReport);
        this.Stoppagesdata = [];

        if (this.stoppagesReport.length != 0) {
          for (var i = 0; i < this.stoppagesReport.length; i++) {

            this.arrivalTime = new Date(this.stoppagesReport[i].arrival_time).toLocaleString();
            this.departureTime = new Date(this.stoppagesReport[i].departure_time).toLocaleString();

            var fd = new Date(this.arrivalTime).getTime();
            var td = new Date(this.departureTime).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            this.Durations = rhours + ':' + rminutes;
            this.Stoppagesdata.push({ 'arrival_time': this.stoppagesReport[i].arrival_time, 'departure_time': this.stoppagesReport[i].departure_time, 'Durations': this.Durations, 'address': this.stoppagesReport[i].address, 'device': this.stoppagesReport[i].device.Device_Name });
            // console.log(this.Stoppagesdata);
          }

        } else {
          let alert = this.alertCtrl.create({
            message: 'No data found!',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.selectedVehicle = undefined;
              }
            }]
          })
          alert.present();
        }


      }, error => {
        this.apicallStoppages.stopLoading();
        console.log(error);
      })
  }

}
