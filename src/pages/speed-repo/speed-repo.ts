import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { Chart } from 'chart.js';
import * as moment from 'moment';
import { Chart } from 'chart.js';

@IonicPage()
@Component({
  selector: 'page-speed-repo',
  templateUrl: 'speed-repo.html',
})
export class SpeedRepoPage implements OnInit {
  @ViewChild('myChartspeed') myChartspeed;
  lineChart: any;
  islogin: any;
  devices: any;
  devices1243: any[];
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  device_id: any;
  SpeedReport: any;
  datetime: string;
  showChartBox: boolean = false;
  datee: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public apiCallspeed: ApiServiceProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    this.datetimeStart = new Date().toISOString();
  }
  ngOnInit() {
    this.getdevices();
  }


  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }


  getdevices() {
    this.apiCallspeed.startLoading().present();
    this.apiCallspeed.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apiCallspeed.stopLoading();
        this.showChartBox = true;
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.devices = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apiCallspeed.stopLoading();
          console.log(err);
        });
  }


  getspeeddevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.device_id = selectedVehicle.Device_ID;
    // this.getIgnitiondeviceReport(from, to);

    console.log("device_id=> ", this.device_id)
    // https://www.oneqlik.in/gps/getGpsSpeedReport?imei=866968030813480&time=2018-10-01T12:34:13.662Z
    var baseURLp = 'http://51.38.175.41/gps/getGpsSpeedReport?imei=' + this.device_id + '&time=' + this.datetimeStart;


    this.apiCallspeed.startLoading().present();
    this.apiCallspeed.getSpeedReport(baseURLp)
      .subscribe(data => {
        this.apiCallspeed.stopLoading();
        this.SpeedReport = data;
        console.log(this.SpeedReport);

        var dataArraySpeed = new Array;
        for (var i = this.SpeedReport.length - 1; i > 0; i--) {
          dataArraySpeed.push(this.SpeedReport[i].speed);
        }
        console.log(dataArraySpeed);
        var dataArrayDate = new Array;
        for (var j = this.SpeedReport.length - 1; j > 0; j--) {
          /*  $scope.datee=($scope.devicesReport[i].time).split('T')[0];*/
          this.datee = moment((this.SpeedReport[j].time)).local().format('h:mm:s a');
          /* console.log($scope.datee);*/
          dataArrayDate.push(this.datee);
        }
        console.log("dataArrayDate=> "+ dataArrayDate);
       
        this.lineChart = new Chart(this.myChartspeed.nativeElement, {
          type: 'line',
          data: {
            datasets: [{
              data: dataArraySpeed,
              label: "Vehicle Speed(Kmph)",
              backgroundColor: "rgb(136,172,161)",
              borderWidth: 1,
              hoverBackgroundColor: "rgba(232,105,90,0.8)",
              hoverBorderColor: "orange",
              scaleStepWidth: 1

            }],

            labels: dataArrayDate
          }
        });
      }, error => {
        this.apiCallspeed.stopLoading();
        console.log(error);
      })
  }


}
